import React, { Component } from "react";

export default class TableUser extends Component {
  renderTbody = () => {
    return this.props.userList.map((user) => {
      return (
        <tr>
          <td>{user.id}</td>
          <td>{user.account}</td>
          <td>{user.username}</td>
          <td>{user.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleUserDelete(user.id);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>

            <button
              onClick={() => {
                this.props.handleUserEdit(user.id);
              }}
              className="btn btn-primary"
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tài khoản</th>
              <th>Họ tên</th>
              <th>Email</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
