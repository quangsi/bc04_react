import _ from "lodash";
import React, { Component } from "react";
import FormUser from "./FormUser";
import TableUser from "./TableUser";

export default class Ex_User_Management extends Component {
  state = {
    userList: [],
    userEdited: null,
  };
  handleUserAdd = (user) => {
    let cloneUserList = [...this.state.userList, user];
    this.setState(
      {
        userList: cloneUserList,
      },
      () => {
        console.log(this.state.userList);
      }
    );
  };
  handleUserDelete = (id) => {
    let index = _.findIndex(this.state.userList, (user) => user.id == id);
    // let index = _.findIndex(this.state.userList, ["id", id]);
    console.log("index: ", index);
  };

  handleUserEdit = (id) => {
    let index = _.findIndex(this.state.userList, (user) => user.id == id);
    if (index == -1) return;

    let userEdited = this.state.userList[index];

    this.setState({ userEdited: userEdited });
  };
  render() {
    return (
      <div className="container p-5">
        <FormUser
          userEdited={this.state.userEdited}
          handleUserAdd={this.handleUserAdd}
        />

        <TableUser
          handleUserEdit={this.handleUserEdit}
          handleUserDelete={this.handleUserDelete}
          userList={this.state.userList}
        />
      </div>
    );
  }
}
