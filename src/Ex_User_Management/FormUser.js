import { nanoid } from "nanoid";
import React, { Component } from "react";
import { initialUserForm } from "./utils";

export default class FormUser extends Component {
  state = {
    user: initialUserForm,
    isEdit: false,
  };

  // static getDerivedStateFromProps(newProps, state) {
  //   console.log("newProps, state: ", newProps, state);
  //   if (newProps.userEdited && !state.isEdit && !state.user.username) {
  //     return {
  //       user: newProps.userEdited,
  //       isEdit: true,
  //     };
  //     // ~ setState
  //   }
  // }
  UNSAFE_componentWillReceveProps(nextProps, state) {
    if (nextProps.userEdited) {
      this.setState({
        user: nextProps.userEdited,
      });
    }
  }

  handleChangeForm = (event) => {
    let { name, value } = event.target;
    this.setState({
      user: { ...this.state.user, [name]: value },
    });
  };

  handleAddUser = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid();
    this.setState({
      user: initialUserForm,
    });
    this.props.handleUserAdd(newUser);
  };
  // "account"
  // account

  render() {
    console.log("this.props.userEdited", this.props.userEdited);
    return (
      <div>
        <form>
          <div class="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.account}
              type="text"
              name="account"
              className="form-control"
              placeholder="Tài khoản"
              aria-describedby="helpId"
            />
          </div>
          <div class="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.username}
              type="text"
              name="username"
              className="form-control"
              placeholder="Họ tên"
              aria-describedby="helpId"
            />
          </div>
          <div class="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.email}
              type="text"
              name="email"
              className="form-control"
              placeholder="Email"
              aria-describedby="helpId"
            />
          </div>
          <button
            type="button"
            onClick={this.handleAddUser}
            className="btn btn-warning"
          >
            Add user
          </button>
        </form>
      </div>
    );
  }
}
