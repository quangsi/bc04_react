import { render } from "@testing-library/react";
import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME, TAI } from "./redux/constants/xucXacContatns";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center pt-5 display-4 text-secondary ">
        <button
          onClick={this.props.handlePlayGame}
          className="btn btn-success "
        >
          <span className="display-4">Play Game</span>
        </button>
        {this.props.luaChon && (
          <p
            className={this.props.luaChon == TAI ? "text-danger" : "text-dark"}
          >
            Bạn chọn : {this.props.luaChon}{" "}
          </p>
        )}
        <p>Số bàn thắng :{this.props.soBanThang} </p>
        <p>Số lượt chơi : {this.props.soLuotChoi}</p>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  luaChon: state.xucXacReducer.luaChon,
  soBanThang: state.xucXacReducer.soBanThang,
  soLuotChoi: state.xucXacReducer.soLuotChoi,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
