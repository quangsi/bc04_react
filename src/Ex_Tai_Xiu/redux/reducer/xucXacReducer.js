import { LUA_CHON, PLAY_GAME, TAI, XIU } from "../constants/xucXacContatns";

const initialState = {
  luaChon: "",
  soBanThang: 0,
  soLuotChoi: 0,
  mangXucXac: [
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
  ],
};

export let xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      state.soLuotChoi++;
      let newMangXucXac = [];
      // score ~ tổng điểm của 3 xúc xắc mới
      let score = 0;
      state.mangXucXac.forEach((item) => {
        let randomNum = Math.floor(Math.random() * 6 + 1);
        score += randomNum;
        let newXucXac = {
          img: `./xuc_xac/${randomNum}.png`,
          giaTri: randomNum,
        };
        newMangXucXac.push(newXucXac);
      });
      //  tính kết quả luot choi

      // if (score >= 11 && state.luaChon == TAI) {
      //   state.soBanThang++;
      // }
      score >= 11 && state.luaChon == TAI && state.soBanThang++;
      score < 11 && state.luaChon == XIU && state.soBanThang++;
      state.mangXucXac = newMangXucXac;
      return { ...state };
    }
    case LUA_CHON: {
      state.luaChon = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
