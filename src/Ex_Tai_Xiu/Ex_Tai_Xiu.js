import React from "react";
import bgGame from "../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export default function Ex_Tai_Xiu() {
  return (
    <div
      style={{
        backgroundImage: `url(${bgGame})`,
        width: "100vw",
        height: "100vh",
      }}
      className="bg_game"
    >
      <XucXac />
      <KetQua />
    </div>
  );
}
