import React, { Component } from "react";
import Header from "./Header";

export default class Demo_LIfeCycle extends Component {
  componentDidMount() {
    // componentDidMount chạy 1 lần duy nhất sau khi render() chạy lần đầu

    // gọi api ở đây
    console.log("App didmount");
  }
  state = {
    like: 1,
    share: 1,
  };
  render() {
    console.log("App render");
    return (
      <div className="text-center">
        {this.state.like < 5 && <Header like={this.state.like} />}
        <div>
          <span className="text-success display-4">{this.state.like}</span>
          <button
            className="btn btn-success"
            onClick={() => {
              this.setState({ like: this.state.like + 1 });
            }}
          >
            Plus like
          </button>
        </div>

        <div>
          <span className="text-primary display-4">{this.state.share}</span>
          <button
            className="btn btn-primary"
            onClick={() => {
              this.setState({ share: this.state.share + 1 });
            }}
          >
            Plus share
          </button>
        </div>
      </div>
    );
  }

  componentDidUpdate() {
    // componentDidUpdate tự động chạy sau khi render() chạy
    console.log("did update");
  }
}
